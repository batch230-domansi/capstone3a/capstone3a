/*
  option 1 preparations:
  - npx create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap

  option 2 preparations
  - npm install -g create-react-app
  - create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap
*/

/*
  Files to delete:
  - App.test.js
  - index.css
  - logo.svg
  - reportWebVitals.js
*/

import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


