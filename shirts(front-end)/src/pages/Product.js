import { Fragment } from 'react';
import { Navigate } from "react-router-dom";
import { useEffect, useState, useContext } from "react";
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import { Container, Row, Col } from 'react-bootstrap';

export default function Product() {
  const { user } = useContext(UserContext);

  const [Product, setProduct] = useState([]);

  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/Product/`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setProduct(data.map(Product =>{
        return(
          <CourseCard key={Product._id} ProductProp={Product}/>
        );
      }));
    })
  }, []);



  return (
    user.isAdmin ? (
      <Navigate to="/admin" />
    ) : (
      <>
        <Container>
          <Row>
            <Col xs={12} md={{ span: 6, offset: 3 }} className="text-center">
              <h1>Offered Program</h1>
            </Col>
          </Row>
          <Row>{Product}</Row>
        </Container>
      </>
    )
  );
}
