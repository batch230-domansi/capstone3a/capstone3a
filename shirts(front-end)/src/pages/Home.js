import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights' ;



export default function Home() {

	const data = {
		title: "Welcome to Lassie Raiment",
		content: "Made with Premium Materials for Ultimate Comfort! ",
		destination: "/",
		newLabel: "Upgrade Your Wardrobe!",
		label: "Shop now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}