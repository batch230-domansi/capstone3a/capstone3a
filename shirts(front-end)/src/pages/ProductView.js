import { useState, useEffect, useContext } from "react";
import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ProductView(){

	const { user } = useContext(UserContext);
	const { ProductId } = useParams();
	const navigate = useNavigate();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [enrollments, setEnrollments] = useState([]);

	useEffect(()=>{
		console.log(ProductId);

		fetch(`${ process.env.REACT_APP_API_URL }/Product/${ProductId}`)
		.then(res => res.json())
		.then(data => {
			
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [ProductId])

	const enroll = (ProductId) =>{

		fetch(`${ process.env.REACT_APP_API_URL }/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				ProductId: ProductId
			})
		})
		.then(res => res.json())
		.then(data => {
			
			console.log("Enroll data: ")
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully Enrolled",
					icon: "success",
					text: "You have successfully enrolled for this course."
				});

				navigate("/Product");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{
								(user.id !== null)
								?
									<Button variant="primary"  size="lg" onClick={() => enroll(ProductId)}>Enroll</Button>
								:
									<Button as={Link} to="/login" variant="success"  size="lg">Login to Enroll</Button>
							}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}