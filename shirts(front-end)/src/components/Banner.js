import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import styles from './Banner.module.css';

export default function Banner({data}){

	console.log(data);
	const {title, content, destination, label, newLabel} = data;

	return (
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p className={styles.content}>{content}</p>
                <p className={styles.newLabel}>{newLabel}</p>
				<Link to={destination}>
				<Button variant="primary">{label}</Button>
				</Link>
			</Col>
		</Row>

	)
}