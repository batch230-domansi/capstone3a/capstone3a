import { useState, useEffect } from "react";
import { Card, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function ProductCard({ProductProp}) {
	const { _id, name, description, price, stocks } = ProductProp;

	return (
	    <Card className="my-3">
	    	<Card.Body>
	    		<Card.Title>
	    			{name}
	    		</Card.Title>
	    		<Card.Subtitle>
	    			Description:
	    		</Card.Subtitle>
	    		<Card.Text>
	    			{description}
	    		</Card.Text>
	    		<Card.Subtitle>
	    			Price:
	    		</Card.Subtitle>
	    		<Card.Text>
	    			Php {price}
	    		</Card.Text>
	    		<Card.Subtitle>
	    			stocks:
	    		</Card.Subtitle>
	    		<Card.Text>
	    			{stocks} available
	    		</Card.Text>
	    		<Button as={Link} to={`/Product/${_id}`} variant="primary">Details</Button>
	    	</Card.Body>
	    </Card>
	)
}