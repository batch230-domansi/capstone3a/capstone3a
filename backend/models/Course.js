

const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	slots:{
		type: Number,
		required: [true, "Slots is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	enrollees : [
		{
			userId : {
				type : String,
				required: [true, "UserId is required"]
			},
			email:{
				type: String,
				required: [true, "Username is required"]
			},
			isPaid:{
				type: Boolean,
				default: true
			},
			dateEnrolled:{
				type: Date,
				default : new Date() 
			}
		}
	]
})

module.exports = mongoose.model("Product", ProductSchema);