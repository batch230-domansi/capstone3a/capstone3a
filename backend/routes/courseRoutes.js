const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

console.log(courseControllers);

router.post("/addCourse", auth.verify, courseControllers.addCourse);

router.get("/all", auth.verify, courseControllers.getAllCourses);

router.get("/", courseControllers.getAllActive);

router.get("/:courseId", courseControllers.getCourse);

router.put("/:courseId", auth.verify, courseControllers.updateCourse);

router.patch("/archive/:courseId", auth.verify, courseControllers.archiveCourse);

module.exports = router;