user {

			id - unique identifier for the document,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin,
			enrollments: [
				{

					id - document identifier,
					ProductId - the unique identifier for the Product,
					ProductName - optional,
					isPaid,
					dateEnrolled
				}
			]

		}


		Product {

			id - unique for the document (auto generated)
			name,
			description,
			price,
			slots,
			isActive,
			createdOn, 
			enrollees: [

				{
					id - document identifier (auto generated),
					userId,
					email,
					isPaid,
					dateEnrolled
				}
			]

		}


		user {

			id - unique identifier for the document,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin

		}

		Product {

			id - unique for the document
			name,
			description,
			price,
			slots,
			isActive

		}

		enrollment {

			id - document identifier,
			userId - the unique identifier for the user,
			ProductId - the unique identifier for the Product,
			ProductName - optional,
			isPaid,
			dateEnrolled

		}