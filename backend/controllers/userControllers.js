const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.getAllUsers = (req, res) =>{
	
	
	return User.find({}).then(result => res.send(result));
	
	
}


module.exports.checkEmailExists = (req, res) =>{
	return User.find({email: req.body.email}).then(result =>{


		console.log(result);


		if(result.length > 0){
			return res.send(true);

		}

		else{
			return	res.send(false);

		}
	})
	.catch(error => res.send(error));
}


module.exports.registerUser = (req, res) =>{

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNumber: req.body.mobileNumber
	})

	console.log(newUser);

	return newUser.save()
	.then(user => {
		console.log(user);

		res.send(true);
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	})
}


module.exports.loginUser = (req, res) =>{
	return User.findOne({email: req.body.email})
	.then(result => {
		if(result == null){
			return res.send(false);
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if(isPasswordCorrect){
				
				return res.send({accessToken: auth.createAccessToken(result)});
			}
			else{
				return res.send(false);				
			}
		}
	})
}


module.exports.getProfile = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result =>{
		result.password = "***";
		res.send(result);
	})
}

module.exports.enroll = async (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	let ProductName = await Product.findById(req.body.ProductId).then(result => result.name);

	let data = {
		userId: userData.id,
		email: userData.email,
		mobileNumber: userData.mobileNumber,
		ProductId: req.body.ProductId,
		ProductName: ProductName
	}

	console.log("data ProductName:");
	console.log(data.ProductName);
	console.log(data.email)

	let isUserUpdated = await User.findById(data.userId)
	.then(user =>{
		user.enrollments.push({
			ProductId: data.ProductId,
			ProductName: data.ProductName,
			mobileNumber: data.mobileNumber
		})

		return user.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	let isProductUpdated = await Product.findById(data.ProductId).then(Product =>{

		Product.enrollees.push({
			userId: data.userId,
			email: data.email
		})
		console.log(data.email)

		Product.slots -= 1;

		return Product.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isProductUpdated);
	(isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false)

}

module.exports.getAllProduct = (req, res) => {
  return User.find({}, { Product: 1, _id: 0 })
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};